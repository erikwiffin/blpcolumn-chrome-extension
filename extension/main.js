(function () {
  var cards, sideboard;

  cards = getCards();
  sideboard = getSideboard();

  function getSideboard() {
    var root = $('#cards_sideboard'),
        rows = root.find('tbody tr:not(.deck_section)'),
        chrome = {total: 0, totalCell: null, prefix: ' | $'},
        cards = [],
        i, card, total, totalCell;

    if ($('#cards_sideboard').length < 1) {
      return;
    }

    // Add table chrome
    chrome.totalCell = $('<span />');
    chrome.totalCell.appendTo('#sideboard_data');
    root.find('thead tr').append('<th class="header">TCG</th>');
    root.find('tfoot tr td:last-child').attr('colspan', '2');

    // Parse out each card
    rows.each(function (index, Element) {
      card = {
        count: parseInt(Element.querySelector('.card_amount').innerText, 10),
        name: Element.querySelector('.cardlink').innerText
      };
      cards.push(card);
      getBlp(card.name).done(function (row, count, chrome) {
        return function (data, textStatus, jqXHR) {
          addColumn(row, count, data.cards, chrome);
        };
      }(Element, card.count, chrome));
    });

    return cards;
  }

  function getCards() {
    var root = document.getElementById('cards'),
        rows = (root) ? root.querySelectorAll('.deck_card') : [],
        chrome = {total: 0, totalCell: null, prefix: '$'},
        cards = [],
        i, card;

    if ($('#cards').length < 1) {
      return;
    }

    // Add table chrome
    chrome.totalCell = $('<td class="deck_main_col_details card_price"></td>');
    chrome.totalCell.appendTo('#cards tr.deck_footer');

    $('.deck_section td').attr('colspan', 8);
    $('#cards thead tr')
      .append('<th class="deck_main_col_details card_price header">TCG</th>');

    // Parse out each card
    for (i=0;i<rows.length;i++) {
      card = {
        count: parseInt(rows[i].querySelector('.card_amount').innerText, 10),
        name: rows[i].querySelector('.cardlink').innerText
      };
      cards.push(card);
      getBlp(card.name).done(function (row, count, chrome) {
        return function (data, textStatus, jqXHR) {
          addColumn(row, count, data.cards, chrome);
        };
      }(rows[i], card.count, chrome));
    }

    return cards;
  }

  function getBlp(card) {
    var url = 'http://mtg.wiffin.com/v1/',
        data = {cards: card};

    return $.getJSON(url, data);
  }

  function addColumn(row, count, data, chrome) {
    var price, finalPrice, i, card, link;

    // No results, get out of here
    if (data.length < 1) {
      return;
    }

    for (i=0;i<data.length;i++) {
      price = parseFloat(data[i].price);

      // Filter out $0 and NaN glitches
      if (isNaN(price) || price <= 0) { 
        continue;
      }

      // Get the cheapest card
      if (!(finalPrice < price)) {
        finalPrice = price;
        card = data[i];
      }
    }

    // Normalize basic lands to 1 cent
    switch (card.name) {
      case "Plains":
      case "Island":
      case "Swamp":
      case "Mountain":
      case "Forest":
        finalPrice = 0.01;
        break;
    }

    finalPrice = finalPrice * count;

    // Update current row
    link = $('<a />').attr('href', card.url)
      .attr('target', '_blank')
      .text('$' + finalPrice.toFixed(2));
    link = $('<td />').addClass('deck_main_col_details card_price')
      .append(link);
    $(row).append(link);

    // Update footer
    chrome.total += finalPrice;
    chrome.totalCell.text(chrome.prefix + chrome.total.toFixed(2));
  }
}());
